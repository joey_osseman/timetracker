# Timetracking

This is a [Brunch](http://brunch.io) powered application 
using [CoffeeScript](http://coffeescript.org),
[Angular](http://angularjs.org), and [AngularUI](http://angular-ui.github.io)
with [Bootstrap 3](http://getbootstrap.com).

## Requirements
1. NodeJS
2. Brunch edge (`npm install -g brunch/brunch`)
3. Bower (`npm install -g bower`)

## Usage

1. Run `brunch w -s` on the checked out folder.
2. Navigate to `localhost:3333` to see it in action, or view `public/` to see the result