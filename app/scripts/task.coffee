class Task
  constructor: (@name, @project, @time) ->
      @timerStart = 0;
      if(!@time)
    	  @time = 0
	     @created = new Date().getTime()
	     @updated = @created
	     @extraWork = false
  start: () -> 
	  @timerStart = new Date().getTime()
	  @updated = new Date().getTime()
	  return
	  
  stop: () ->
	  @time = @getTime()
	  @timerStart = 0
	  @updated = new Date().getTime()
	  return
	  
  isRunning: () -> @timerStart != 0
  
  getFormattedTime: () -> 
  	time = @getTime()
  	seconds = Math.floor time / 1000 % 60
  	minutes = Math.floor time / 60000 % 60
  	hours = Math.floor time / 3600000
  	
  	if(seconds < 10)
  		seconds = "0" + seconds
  	if(minutes < 10)
  		minutes = "0" + minutes
  	if(hours < 10)
  		hours = "0" + hours
  	
  	"#{hours}:#{minutes}:#{seconds}"
  
  setProject: (project) -> 
  	  @project = project
  	  @updated = new Date().getTime()
  	  return
    
  getTime:() -> @time + if @isRunning() then new Date().getTime() - @timerStart  else 0
  
  getCosts: (hourPrice) -> if(@extraWork) then Math.ceil(Math.ceil( @getTime() / 60000 / 15)*(hourPrice/4)*100)/100 else "-";
  getFormattedCreateTime: () -> new Date(@created).toPrettyString()