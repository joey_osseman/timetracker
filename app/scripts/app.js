var app = angular.module('HourTracking', [ 'ngRoute' ]);
var scope = null;
app.controller('default', function($scope, $timeout, $route, taskStorage) {
	scope = $scope;
	$scope.projects = [ new Project("Cofano"), new Project("Other") ];
	$scope.tasks = taskStorage.get();
	$scope.newTask = new Task(null, $scope.projects[0]);
	$scope.predicate = "created";
	$scope.reverse = true;
	$scope.filter = { project: { name: "" } };
	$scope.database = {
			emptyDatabase: function(){
				if(confirm("Are you sure?"))
					$scope.tasks = [];
			},
			exportDatabase: function(){
				this.exportData = angular.toJson($scope.tasks);
			},
			importDatabase: function(){
				var result = JSON.parse(this.importData || '[]');
				for (var i = 0; i < result.length; i++) {
					result[i] = Object.cast(result[i], Task);
				}
				$scope.tasks = result;
				this.isImporting =  false;
			},
			isImporting: false,
			isExporting: false,
			importData : "",
			exportData : ""
	};
	$scope.$watch('tasks', function(a, b, c) {
		taskStorage.put($scope.tasks);
	}, true);

	$scope.addTask = function() {
		$scope.tasks.push($scope.newTask);
		$scope.newTask = new Task(null, $scope.newTask.project);
	};
	$scope.removeTask = function(task) {
		if(confirm("Are you sure?"))
		$scope.tasks.splice($scope.tasks.indexOf(task), 1);

	};
	$scope.startTask = function(task) {
		if (task.isRunning()) {
			task.stop();
		} else {
			task.start();
		}
	};
	$scope.onTimeout = function() {
		$route.reload();
		$timeout($scope.onTimeout, 1000);
	};
	$timeout($scope.onTimeout, 1000);
});
app.factory('taskStorage', function() {
	var STORAGE_ID = 'tasks';

	return {
		get : function() {
			// console.log("getting");

			var result = JSON.parse(localStorage.getItem(STORAGE_ID) || '[]');
			for (var i = 0; i < result.length; i++) {
				result[i] = Object.cast(result[i], Task);
			}
			// console.log(result);
			console.log("");
			return result;
		},

		put : function(tasks) {
			localStorage.setItem(STORAGE_ID, angular.toJson(tasks));
		}
	};
});
/*
 * function save(tasks) { localStorage["tasks"] = JSON.stringify(tasks); }
 * 
 * function load() { return JSON.parse(localStorage["tasks"]); /*var array = [];
 * if(localStorage["tasks"]) { var arrayLoaded =
 * JSON.parse(localStorage["tasks"]); for(var i = 0; i < arrayLoaded.length;
 * i++) { array.push(Object.cast(arrayLoaded[i], Task)) } } console.log(array);
 * return array; }
 */
Object.cast = function(rawObj, constructor) {
	var obj = new constructor();
	for ( var i in rawObj) {
		obj[i] = rawObj[i];
		if(i == "created")
			console.log(obj[i])
	}
	return obj;
}

Array.prototype.groupBy = function(x) {
	if (typeof (v) == "function") {
		var result = [];
		for (var i = 0; i < this.length; i++) {
			var value = x(this[i]);
			if (!result[value]) {
				result[value] = [];
			}

			result[value].push(this[i]);
		}
	} else {
		var navigationProperties = x.split(".");
		var getNavigationProperty = function(x) {
			for (var i = 0; i < navigationsProperties; i++)
				x = x[navigationProperties[i]];
		};
		return this.groupBy(getNavigationProperty);
	}
};

Date.prototype.toPrettyString = function() {

	    var d = this.getDate();
	    var m = this.getMonth() + 1;
	    var y = this.getFullYear();
	    var h = this.getHours();
	    var min = this.getMinutes();
	    
	    return ""+ (h <= 9 ? '0' + h : h) + ":" + (min <= 9 ? '0' + min : min) + " "+  (d <= 9 ? '0' + d : d) + '-' + (m<=9 ? '0' + m : m) + "-" + y; ;
};
